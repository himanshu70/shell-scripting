#!/bin/bash  -x
i=0
IFS='*'
passwd='craterzone'
user='root'
set -x

add_device_info() {
user_id="$1"
user_agent="$2"
app_type="$3"
login_date="$4"
old_user_id="$5"

device_token=$(mysql -u $user -p$passwd -Dwedoshoes -ss -N  -e"select push_token from deviceinfo  where user_id='$old_user_id'" )

mysql -u $user -p$passwd -Dwedoshoes_core  -e "INSERT INTO device_info(user_id,user_agent,device_token,device_type,login_date) values ('$user_id','$user_agent','$device_token','$app_type','$login_date')"
}



#For Creating Cart At the Time Of Registration
create_cart() {
max_id="$1"
create_date="$2"
active=1
size=0

mysql -u $user -p$passwd -Dwedoshoes_oms  -e "INSERT INTO cart  (user_id,create_date,active,size) values ('$max_id','$create_date','$active','$size')"
}

#Creates Wallet At the Time Of Registration
create_wallet() {
max_id="$1"
balance=0.00
created_date="$2"
active=1

mysql -u $user -p$passwd -Dwedoshoes_wallet  -e "INSERT INTO wallet  (user_id,balance,created_date,active) values ('$max_id','$balance','$created_date','$active')"
}

#Updates Address At The Time Of Registration
add_address(){

address_type="$1"
address="$2"
max_id="$3"
name="$4"
last_name="$5"
phone="$6"
em="$7"
last_login_time="$8"


locality='Un-Defined'
latitude='NULL'
longitude='NULL'
city='NULL'
pin='NULL'
state='NULL'
landmark='NULL'
type_address=3
created_date="$last_login_time"

echo "$address_type"
echo "$address"
if [ "$address_type" -eq "1" ]
then address_type="HOME"
elif [ "$address_type" -eq "2" ]
then address_type="WORK"
else address_type="OTHER"
fi
echo "$address"

mysql -u $user -p$passwd -Dwedoshoes_core  -e "INSERT INTO addresses (user_id,name,last_name,cc,phone,email_id,street_address,type_address,city,state,pin,landmark,address_type,locality,created_date) values ('$max_id','$name','$last_name','91','$phone','$em','$address','$type_address','$city','$state','$pin','$landmark','$address_type','$locality','$created_date')"

}


while read user_id email name gender image_url last_login_time phone social_id  
do
echo "***********************************************************************************"
last_name=' '
echo "$user_id"
echo "$email"
echo "$name"
#password is wedoshoes
password='$2a$12$Zp3zQOtFQvqGrNNYZs9kEufPNu7l5VYtmzoiBnki/ArAXU/HMY276'

## FOR CURRENT TIMESTAMP FOR REGISTRATION DATE 
register_date=`date +%s`

echo "$gender"
if [ "$gender" == "NULL" ]
then gender='U'
elif [ "$gender" ==  "MALE" ]
then gender='M'
elif [ "$gender" ==  "FEMALE" ]
then gender='F'
else  gender='U'
fi

#FOR REGISTERING USERS INTO NEW WEDOSHOES_CORE
mysql -u $user -p$passwd -Dwedoshoes_core --skip-column-names -e"INSERT into users(email_id,name,last_name,gender,image_url,user_type,cc,phone,password,active,contact_mode_id,register_date)  values('$email','$name','$last_name','$gender','$image_url',1,'91', $phone,'$password',1,6,'$last_login_time');"
echo "$$$$$$$$$$$$$$$$$$$$$$$$"
echo "$email"
max_id=$(mysql -u $user -p$passwd -Dwedoshoes_core -ss -N  -e"select MAX(id) from users where email_id='$email'")
echo "$max_id"

# FOR UPDATING COMMUNICATION SETTINGS OF ALL THE USERS BEING REGISTERED
mysql -u $user -p$passwd -Dwedoshoes_core  --skip-column-names -e"INSERT INTO communication_settings(user_id,sms,email) values("$max_id",1,1);"                                                  
create_wallet $max_id $last_login_time

create_cart $max_id $last_login_time
while read  device_id app_type creation_date_time
do
add_device_info $max_id $device_id $app_type $creation_date_time $user_id

done < <(mysql -u $user -p$passwd -Dwedoshoes -ss -N --skip-column-names -e"SELECT CONCAT(IFNULL(SUBSTRING(unique_id,21,14),' '),'*',IFNULL(app_type,' '),'*',IFNULL(UNIX_TIMESTAMP(creation_date_time),' ')) FROM token where user_id='$user_id';")

##NOW FOR ADDRESS UPDATE

while read address_type address em
do

add_address $address_type $address $max_id $name $last_name $phone $em $last_login_time 

done < <(mysql -u $user -p$passwd -Dwedoshoes -ss -N --skip-column-names -e"SELECT CONCAT(IFNULL(address_type,' '),'*',IFNULL(address,' '),'*',IFNULL(email,' ')) FROM useraddress where user_id='$user_id';")

done < <(mysql --user $user -p$passwd -Dwedoshoes --skip-column-names -e"select GROUP_CONCAT(user_id,'*',email,'*',name,'*',IFNULL(gender,' '),'*',IFNULL(image_url,' '),'*',IFNULL(UNIX_TIMESTAMP(last_login_time),' '),'*',IFNULL(phone,' '),'*',social_id) from user where phone IS NOT NULL group by phone;")
